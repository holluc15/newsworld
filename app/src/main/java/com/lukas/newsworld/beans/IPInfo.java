package com.lukas.newsworld.beans;

public class IPInfo {
    /*
    newsworld - IPInfo
    Author: Lukas Holzmann
    Datum: 13.Mai 2019
    */
    private String country = "";
    private String countryCode = "";

    public IPInfo(String country, String countryCode) {
        this.country = country;
        this.countryCode = countryCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
