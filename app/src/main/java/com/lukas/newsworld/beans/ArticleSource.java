package com.lukas.newsworld.beans;

import java.io.Serializable;

public class ArticleSource implements Serializable {
    /*
    newsworld - ArticleSource
    Author: Lukas Holzmann
    Datum: 13.Mai 2019
    */
    private String name = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
