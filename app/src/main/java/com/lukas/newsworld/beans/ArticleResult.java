package com.lukas.newsworld.beans;

import java.util.List;

public class ArticleResult {
    /*
    newsworld - ArticleResult
    Author: Lukas Holzmann
    Datum: 13.Mai 2019
    */
    private String status = "";
    private int totalResults = 0;
    private List<com.lukas.newsworld.beans.Article> articles = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public List<com.lukas.newsworld.beans.Article> getArticles() {
        return articles;
    }

    public void setArticles(List<com.lukas.newsworld.beans.Article> articles) {
        this.articles = articles;
    }
}
