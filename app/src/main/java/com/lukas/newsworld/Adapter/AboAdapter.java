package com.lukas.newsworld.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.newsworld.R;
import com.lukas.newsworld.MainActivity;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Random;
import java.util.Set;


public class AboAdapter extends RecyclerView.Adapter<AboAdapter.ViewHolder> {
/*
    newsworld - AboAdapter
    Author: Lukas Holzmann
    Datum: 23.April 2019
 */

    private LinkedList<String> mData;
    private LayoutInflater mInflater;
    private MyRecyclerViewAdapter.ItemClickListener mClickListener;
    private int selectedPos = RecyclerView.NO_POSITION;
    private String[] colors;
    private Context context;
    private final String key = "ABOS_KEY";
    private AboAdapter adapter;
    private MainActivity main;

    // data is passed into the constructor
    public AboAdapter(Context context, Set<String> data, MainActivity main) {
        this.mInflater = LayoutInflater.from(context);
        mData = new LinkedList<>(data);
        this.context = context;
        adapter = this;
        this.main = main;
        //addColors();
    }

    private void addColors() {
        colors = new String[5];
        colors[0] = "#12355B";
        colors[1] = "#420039";
        colors[2] = "#D72638";
        colors[3] = "#FFB238";
        colors[4] = "#FF570A";
    }

    // inflates the row layout from xml when needed
    @Override
    public AboAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.abo_item, parent, false);
        return new AboAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(AboAdapter.ViewHolder holder, int position) {
        String abo = mData.get(position);

        holder.aboText.setText(abo);
        holder.closeButton.setOnClickListener(v -> {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPref.edit();
            mData.remove(position);

            editor.putStringSet(key, new HashSet<>(mData));
            editor.commit();
            adapter.notifyDataSetChanged();

        });

        holder.aboCard.setOnClickListener(v -> {
            main.searchForArticle(abo);
        });

    }

    private String getRandomColor() {
        Random rand = new Random();
        return colors[rand.nextInt(5)];
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView aboText;
        CardView aboCard;
        ImageButton closeButton;

        ViewHolder(View itemView) {
            super(itemView);
            aboText = itemView.findViewById(R.id.abo_text);
            aboCard = itemView.findViewById(R.id.abo_card);
            closeButton = itemView.findViewById(R.id.close_button_abo_item);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                notifyItemChanged(selectedPos);
                selectedPos = getLayoutPosition();
                notifyItemChanged(selectedPos);
                mClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }

    // convenience method for getting data at click position
    String getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(MyRecyclerViewAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setmData(HashSet<String> set) {
        mData.clear();
        mData.addAll(set);
        this.notifyDataSetChanged();
    }


}
