package com.lukas.newsworld.Adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.newsworld.R;
import com.lukas.newsworld.activities.Activity_Beitrag;
import com.lukas.newsworld.beans.Article;
import com.squareup.picasso.Picasso;

import java.util.LinkedList;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {
    /*
        newsworld - MyRecyclerViewAdapter
        Author: Lukas Holzmann
        Datum: 10.April 2019
     */
    private LinkedList<Article> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private int selectedPos = RecyclerView.NO_POSITION;
    private String[] colors;

    // data is passed into the constructor
    public MyRecyclerViewAdapter(Context context, LinkedList<Article> data, RelativeLayout drawerPane) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        addColors();
        setClickListener((view, position) -> {

            if (!drawerPane.isShown()) {
                Intent intent = new Intent(context, Activity_Beitrag.class);
                intent.putExtra("BEITRAG_ID", data.get(position));
                context.startActivity(intent);
            }

        });

    }

    private void addColors() {
        colors = new String[5];
        colors[0] = "#12355B";
        colors[1] = "#420039";
        colors[2] = "#D72638";
        colors[3] = "#FFB238";
        colors[4] = "#FF570A";
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.beitrag_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Article article = mData.get(position);
        holder.headlineTextView.setText(article.getTitle());
        String time = article.getPublishedAt();
        holder.timeTextView.setText(time);
        holder.timeTextView.setBackgroundColor(Color.parseColor(getRandomColor()));
        holder.publisher.setText(article.getSource().getName());
        String logo = article.getUrlToImage();
        if (logo != null) {
            if (!logo.isEmpty()) {
                Picasso.get().load(logo).placeholder(R.drawable.ic_launcher_foreground).error(R.drawable.ic_launcher_background).into(holder.imageView);
            } else {
                holder.imageView.setImageResource(R.drawable.ic_launcher_background);
            }
        } else {
            holder.imageView.setImageResource(R.drawable.ic_launcher_background);
        }

    }

    private String getRandomColor() {
        Random rand = new Random();
        return colors[rand.nextInt(5)];

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView headlineTextView;
        TextView timeTextView;
        TextView publisher;

        CircleImageView imageView;
        ViewHolder(View itemView) {
            super(itemView);
            headlineTextView = itemView.findViewById(R.id.abo_text);
            timeTextView = itemView.findViewById(R.id.timeRoundedRect);
            imageView = itemView.findViewById(R.id.newsImage);
            publisher = itemView.findViewById(R.id.publisher);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                notifyItemChanged(selectedPos);
                selectedPos = getLayoutPosition();
                notifyItemChanged(selectedPos);
                mClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }

    // convenience method for getting data at click position
    Article getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}