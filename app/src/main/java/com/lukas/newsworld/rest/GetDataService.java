package com.lukas.newsworld.rest;


import com.lukas.newsworld.beans.ArticleResult;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;


public interface GetDataService {

    /*
    newsworld - GetDataService
    Author: Lukas Holzmann
    Datum: 17.April 2019
    */

    @GET("top-headlines")
    Observable<Response<ArticleResult>> getArticles(@QueryMap Map<String, String> options);

    @GET("everything")
    Observable<Response<ArticleResult>> searchForArticle(@QueryMap Map<String, String> options);
}
