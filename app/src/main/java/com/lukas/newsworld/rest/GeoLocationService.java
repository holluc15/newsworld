package com.lukas.newsworld.rest;


import com.lukas.newsworld.beans.IPInfo;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;

public interface GeoLocationService {

    /*
    newsworld - GeoLocationService
    Author: Lukas Holzmann
    Datum: 17.April 2019
    */
    @GET("json")
    Observable<Response<IPInfo>> getLocation();
}
