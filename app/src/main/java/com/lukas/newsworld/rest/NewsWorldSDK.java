package com.lukas.newsworld.rest;

import com.squareup.moshi.KotlinJsonAdapterFactory;
import com.squareup.moshi.Moshi;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class NewsWorldSDK {
   /*
    newsworld - NewsWorldSDK
    Author: Lukas Holzmann
    Datum: 17.April 2019
    */

   private String apiurl = "https://newsapi.org/v2/";
   public static String token = null;
   private OkHttpClient httpClient;
   public com.lukas.newsworld.rest.GetDataService service;

   public NewsWorldSDK() {
      HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
      logging.setLevel(HttpLoggingInterceptor.Level.BODY);

      Moshi moshi = new Moshi.Builder().add(new KotlinJsonAdapterFactory()).build();

      Retrofit.Builder builder = new Retrofit.Builder();
      builder.baseUrl(apiurl);
      builder.addConverterFactory(ScalarsConverterFactory.create());
      builder.addConverterFactory(MoshiConverterFactory.create(moshi));
      builder.addCallAdapterFactory(RxJavaCallAdapterFactory.create());
      builder.addCallAdapterFactory(RxJava2CallAdapterFactory.create());

      OkHttpClient.Builder okBuilder = new OkHttpClient.Builder();
      okBuilder.addInterceptor(new Interceptor() {
         @Override
         public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();
            Request request = original.newBuilder()
                    .header("Content-Type", "application/json;charset=utf8")
                    .header("Accept", "application/json")
                    .header("apiKey", token)
                    .method(original.method(),original.body()).build();

            return chain.proceed(request);
         }
      });

      okBuilder.addInterceptor(logging);
      okBuilder.connectTimeout(50, TimeUnit.SECONDS);
      okBuilder.readTimeout(50,TimeUnit.SECONDS);

      httpClient = okBuilder.build();
      builder.client(httpClient);

      Retrofit retrofit = builder.build();
      service = retrofit.create(com.lukas.newsworld.rest.GetDataService.class);

   }

}
