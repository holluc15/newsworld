package com.lukas.newsworld.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;

import com.example.newsworld.R;
import com.lukas.newsworld.MainActivity;
import com.lukas.newsworld.beans.Article;

public class Activity_Beitrag extends Activity {
    /*
        newsworld - ActivityBeitrag
        Author: Lukas Holzmann
        Datum: 25.April 2019
     */
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beitrag);
        Intent i = getIntent();
        Article article = (Article) i.getSerializableExtra("BEITRAG_ID");

        // Use a CustomTabsIntent.Builder to configure CustomTabsIntent.
        String url = article.getUrl();
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        // set toolbar color and/or setting custom actions before invoking build()
        // Once ready, call CustomTabsIntent.Builder.build() to create a CustomTabsIntent
        CustomTabsIntent customTabsIntent = builder.build();
        // and launch the desired Url with CustomTabsIntent.launchUrl()
        customTabsIntent.launchUrl(this, Uri.parse(url));

    }

    @Override
    public void onBackPressed() {
        this.startActivity(new Intent(this, MainActivity.class));
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        finish();
    }
}
