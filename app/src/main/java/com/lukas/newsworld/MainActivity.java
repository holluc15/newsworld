package com.lukas.newsworld;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.newsworld.R;
import com.lukas.newsworld.Adapter.AboAdapter;
import com.lukas.newsworld.Adapter.MyRecyclerViewAdapter;
import com.lukas.newsworld.beans.Article;
import com.lukas.newsworld.beans.ArticleResult;
import com.lukas.newsworld.beans.ChachedData;
import com.lukas.newsworld.beans.IPInfo;
import com.lukas.newsworld.rest.GeoLocationSDK;
import com.lukas.newsworld.rest.GeoLocationService;
import com.lukas.newsworld.rest.GetDataService;
import com.lukas.newsworld.rest.NewsWorldSDK;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Set;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
/*
    newsworld - MainActivity
    Author: Lukas Holzmann
    Datum: 10.April 2019
 */

    private GetDataService service = new NewsWorldSDK().service;
    private GeoLocationService geoLocationService = new GeoLocationSDK().service;
    private String api_key = "06ebf69547694cddae9f79ec678e6fe8";
    private LinkedList<Article> articles;
    private RecyclerView newsList;
    private RecyclerView navList;
    private LinearLayoutManager layoutManager;
    private LinearLayoutManager layoutManagerAbo;
    public Context context;
    public MyRecyclerViewAdapter adapter;
    public AboAdapter aboAdapter;
    public Set<String> abos;
    private final String abokey = "ABOS_KEY";
    private String language;
    private final String langkey = "LANG_KEY";
    private String countryCode;
    private String filter = "";

    private TextView tv;
    private int checkedItem = 0;
    SharedPreferences sharedPref;
    Locale myLocale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;
        articles = new LinkedList<>();
        abos = new HashSet<>();
        newsList = findViewById(R.id.newsList);
        navList = findViewById(R.id.navList);

        this.getWindow().setNavigationBarColor(Color.parseColor("#040f16"));

        layoutManager = new LinearLayoutManager(context);
        layoutManagerAbo = new LinearLayoutManager(context);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        navList.setLayoutManager(layoutManagerAbo);


        language = sharedPref.getString(langkey, "standard");
        abos.addAll(sharedPref.getStringSet(abokey, new HashSet<String>()));
        aboAdapter = new AboAdapter(context, abos, this);
        navList.setAdapter(aboAdapter);


        NewsWorldSDK.token = api_key;
        tv = findViewById(R.id.toolbarTextView);

        checkOnline();
        initButtons();


    }

    public void checkOnline() {
        if (isOnline()) {
            if (ChachedData.chachedArticles.size() == 0) {
                getLocation();
            } else {
                newsList.setLayoutManager(layoutManager);
                articles.addAll(ChachedData.chachedArticles);
                adapter = new MyRecyclerViewAdapter(context, articles, findViewById(R.id.drawerPane));
                newsList.setAdapter(adapter);
            }
        } else {
            TextView tv = findViewById(R.id.toolbarTextView);
            tv.setText(this.getText(R.string.offline));
        }
    }

    public void initButtons() {

        //Initialize menu button
        DrawerLayout nav_layout = findViewById(R.id.drawerLayout);
        ImageButton menuButton = findViewById(R.id.menu_button);
        menuButton.setOnClickListener(v -> nav_layout.openDrawer(Gravity.LEFT));

        SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.swipe_refresh_list);
        swipeRefreshLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        swipeRefreshLayout.setProgressViewOffset(false, 0, 100);
        swipeRefreshLayout.setOnRefreshListener(() -> new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                getLocation();
            }

        }, 4000));

        //Initialize search button
        SearchView searchView = findViewById(R.id.searchButton);

        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                searchForArticle(s);
                searchView.setQuery("", true);
                searchView.setIconified(true);

                newsList.scrollToPosition(0);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                System.out.println(s);
                return true;
            }
        });

        //Initialize language button
        LinearLayout languages_button = findViewById(R.id.languages_button);
        languages_button.setOnClickListener(v -> {
            final String[] items = {getString(R.string.english), getString(R.string.german), getString(R.string.russian)};

            AlertDialog.Builder builder = new AlertDialog.Builder(context);

            builder.setTitle(getString(R.string.selection))
                    .setSingleChoiceItems(items, -1, (dialog, item) -> {
                                checkedItem = item;
                            }

                    );

            builder.setPositiveButton(getString(R.string.ok), (dialog, item) -> {
                language = items[checkedItem];
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(langkey, language);
                editor.commit();


                TextView tv = findViewById(R.id.toolbarTextView);


                getArticles(getCountryCodeOfLanguage(language));
                tv.setText(String.format("newsworld - %s", getCountryCodeOfLanguage(language).toUpperCase()));


            });
            builder.setNegativeButton(getString(R.string.cancel), null);

            builder.show();
        });


        ImageButton aboButton = findViewById(R.id.add_abo_button);
        aboButton.setOnClickListener(v -> {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View formElementsView = inflater.inflate(R.layout.abo_dialog, null, false);
            final EditText nameEditText = formElementsView.findViewById(R.id.theme);

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setView(formElementsView)
                    .setTitle(getString(R.string.add_theme))
                    .setPositiveButton(getString(R.string.ok), (dialog, which) -> {
                        String text = nameEditText.getText().toString();
                        Toast.makeText(context, "✔", Toast.LENGTH_SHORT).show();

                        abos.clear();
                        abos.addAll(sharedPref.getStringSet(abokey, new HashSet<String>()));

                        abos.add(text);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putStringSet(abokey, abos);
                        editor.commit();

                        aboAdapter.setmData(new HashSet<>(abos));


                    })
                    .setNegativeButton(R.string.cancel, null)
                    .show();
        });

        // clear topics button
        LinearLayout clear_button = findViewById(R.id.clear_button);
        clear_button.setOnClickListener(v -> {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putStringSet(abokey, new HashSet<>());
            editor.commit();
            aboAdapter.setmData(new HashSet<>());
            Toast.makeText(context, "cleared Tags ✔", Toast.LENGTH_SHORT).show();
        });

        LinearLayout filter_button = findViewById(R.id.filter_button);
        filter_button.setOnClickListener(v -> {
            final String[] filters = {getString(R.string.business), getString(R.string.entertainment), getString(R.string.general), getString(R.string.health), getString(R.string.science), getString(R.string.sports), getString(R.string.technology)};
            AlertDialog.Builder builder = new AlertDialog.Builder(context);

            builder.setTitle(getString(R.string.filter)).setIcon(R.drawable.ic_arrow_back_black_24dp);
            builder.setSingleChoiceItems(filters, -1, (dialog, item) -> {
                checkedItem = item;

            });
            builder.setPositiveButton(getString(R.string.ok), (dialog, which) -> {
                filter = filters[checkedItem];
                getArticles(getCountryCodeOfLanguage(language));
                tv.setText(String.format("newsworld - %s", getCountryCodeOfLanguage(language).toUpperCase()));
            });
            builder.setNegativeButton(getString(R.string.cancel), null);

            builder.show();
        });
    }


    public String getCountryCodeOfLanguage(String language) {
        switch (language) {
            case "English":
                return "us";
            case "German":
                return "de";
            case "Russian":
                return "ru";
            default:
                return "de";
        }

    }


    @SuppressLint("CheckResult")
    public void searchForArticle(String searchText) {
        HashMap<String, String> options = new HashMap<>();
        options.put("apiKey", api_key);
        options.put("q", searchText);
        options.put("pageSize", "50");

        //options.put("sortBy", "relevancy");
        //options.put("language", language);

        service.searchForArticle(options).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableObserver<Response<ArticleResult>>() {

            @Override
            public void onNext(Response<ArticleResult> articleResultResponse) {
                if (articles.size() == 0) {
                    Toast.makeText(context, getString(R.string.nothing_found), Toast.LENGTH_SHORT).show();
                } else {
                    articles.clear();
                    articles.addAll(articleResultResponse.body().getArticles());
                    setTimeOfArticle();
                }


            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

                newsList.getAdapter().notifyDataSetChanged();
            }
        });

    }

    public boolean isOnline() {
        /*
         *  Check if System is online by simply using ping to google DNS (8.8.8.8)
         */
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        }
        catch (IOException e)          { e.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }

        return false;
    }

    @SuppressLint("CheckResult")
    public void getArticles(String countrycode) {
        /*
         * This method gets all local articles from the newsapi.org
         */
        HashMap<String, String> options = new HashMap<>();
        options.put("apiKey",api_key);
        if (language.equals("standard")) {
            options.put("country", countrycode);
            tv.setText(String.format("newsworld - %s", countrycode.toUpperCase()));

        } else {
            options.put("country", getCountryCodeOfLanguage(language));
            System.out.println(getCountryCodeOfLanguage(language));
            tv.setText(String.format("newsworld - %s", getCountryCodeOfLanguage(language).toUpperCase()));
        }
        if (!filter.isEmpty()) {
            options.put("category", filter);
        }

        options.put("pageSize", "50");
        //options.put("sortBy", "publishedAt");
        //options.put("language", language);
        service.getArticles(options).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableObserver<Response<ArticleResult>>() {
            @Override
            public void onNext(Response<ArticleResult> articleResultResponse) {

                if (articleResultResponse.body() != null) {
                    articles.clear();
                    articles.addAll(articleResultResponse.body().getArticles());
                    ChachedData.chachedArticles.addAll(articles);
                    setTimeOfArticle();
                }

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                if (newsList.getAdapter() == null) {
                    newsList.setLayoutManager(layoutManager);
                    newsList.setAdapter(new MyRecyclerViewAdapter(context, articles, findViewById(R.id.drawerPane)));
                } else {
                    newsList.getAdapter().notifyDataSetChanged();
                }
            }
        });

    }

    public void setTimeOfArticle() {
        for(Article article : articles) {
            String dateTime = article.getPublishedAt();
            String[] parts = dateTime.split("T");
            String[] time = parts[1].split(":");
            if (time.length >= 1) {
                article.setPublishedAt(time[0] + ":" + time[1]);
            }

        }
    }

    @SuppressLint("CheckResult")
    public void getLocation() {
        /*
         * This method gets the location of the device by simply calling http://ip-api.com/
         */
        geoLocationService.getLocation().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableObserver<Response<IPInfo>>() {

            @Override
            public void onNext(Response<IPInfo> ipInfoResponse) {
                IPInfo ipInfo = ipInfoResponse.body();
                countryCode = ipInfo.getCountryCode();
                TextView tv = findViewById(R.id.toolbarTextView);
                tv.append(" - " + countryCode);
                countryCode = countryCode.toLowerCase();
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                getArticles(countryCode);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        System.out.println("Not able to press back anymore!");
    }


}
